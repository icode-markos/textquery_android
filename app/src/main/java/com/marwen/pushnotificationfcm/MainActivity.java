package com.marwen.pushnotificationfcm;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity  {

    private static final int REQUEST_CODE_PERMISSION_SEND_SMS = 123;
    private final static int REQUEST_CODE_PERMISSION_READ_SMS = 456;
    private static final String TAG = "0";
    static final int READ_BLOCK_SIZE = 100;

    String Number, Message;
    String str, mes, dates;
    Context mContext;

    ArrayList<String> smsMsgList = new ArrayList<String>();
    ArrayAdapter arrayAdapter;
    ListView lvSMS;

    public static MainActivity instance;
    public static MainActivity Instance(){
        return  instance;
    }

    @Override
    public void onStart() {
        super.onStart();
        instance = this;
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("MyData")
        );

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        instance = this;
        mContext = this;

//
//        checkConnection();

        if(ContextCompat.checkSelfPermission(getBaseContext(), "android.permission.READ_SMS") == PackageManager.PERMISSION_GRANTED) {
//            refreshInbox();
        } else {
            final int REQUEST_CODE_ASK_PERMISSIONS = 123;
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{"android.permission.READ_SMS"}, REQUEST_CODE_ASK_PERMISSIONS);
        }



        //FIREBASE INSTANCE
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        String token = task.getResult().getToken();
                        Log.d(TAG, token);
                    }
                });

        //FRAGMENT ROUTING
        BottomNavigationView navigationView = findViewById(R.id.btm_nav);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.dashboard:
                        DashFragment dashfragment = new DashFragment();
                        FragmentTransaction dashfragmentTransaction = getSupportFragmentManager().beginTransaction();
                        dashfragmentTransaction.replace(R.id.frame_layout, dashfragment);
                        dashfragmentTransaction.commit();
                        return true;
                    case R.id.sent:
                        SentFragment sentfragment = new SentFragment();
                        FragmentTransaction sentfragmentTransaction = getSupportFragmentManager().beginTransaction();
                        sentfragmentTransaction.replace(R.id.frame_layout, sentfragment);
                        sentfragmentTransaction.commit();
                        return true;
                    case R.id.recieved:
                        ReceivedFragment recfragment = new ReceivedFragment();
                        FragmentTransaction recfragmentTransaction = getSupportFragmentManager().beginTransaction();
                        recfragmentTransaction.replace(R.id.frame_layout, recfragment);
                        recfragmentTransaction.commit();
                        return true;
                }

                return false;
            }
        });
        deleteFile("send12.txt");
        navigationView.setSelectedItemId(R.id.dashboard);
    }
    //
//    private void checkConnection() {
//        boolean isConnected = ConnectivityReceiver.isConnected();
//        showSnack(isConnected);
//    }
//
//    private void showSnack(boolean isConnected) {
//        if (isConnected) {
//            try{
//                Cursor cursor = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
//                if (cursor.moveToFirst()) {
//                    sendtodatabase();
//                }
//                else{
//                    Toast.makeText(getApplicationContext(),"Message is empty", Toast.LENGTH_LONG).show();
//                }
//
//            }catch (Exception e){
//                Toast.makeText(getApplicationContext(),"Error " + " " + e, Toast.LENGTH_SHORT).show();
//            }
//        } else {
//            Toast.makeText(getApplicationContext()," No internet connection", Toast.LENGTH_SHORT).show();
//        }
//    }
//




    //INTENT BROADCAST FOR RECEIVING DATA
    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, intent.getExtras().getString("msg"), Toast.LENGTH_SHORT).show();
            jsonToString(context, intent.getExtras().getString("msg"));
        }
    };

    //SEND FUNCTION
    public void jsonToString(Context context, String jsonData) {
        Toast.makeText(context, "Json Process", Toast.LENGTH_SHORT).show();
        try {
            JSONArray arr = new JSONArray(jsonData);
            for(int i = 0; i < arr.length(); i++){
                writeToFile(context, arr.getJSONObject(i).getString("number") + " : " + arr.getJSONObject(i).getString("message") + " : " + arr.getJSONObject(i).getString("sim")+ " : " + arr.getJSONObject(i).getString("attempt"));
            }
            ReadFile(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeToFile(Context context, String data) {
        try {
            FileOutputStream fileinput = context.openFileOutput("send12.txt", context.MODE_APPEND);
            OutputStreamWriter outputWriter=new OutputStreamWriter(fileinput);
            outputWriter.append(data +"\n");
            outputWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ReadFile(Context context) {
        Toast.makeText(context, "Read Process", Toast.LENGTH_SHORT).show();
        try {
            FileInputStream fileIn = context.openFileInput("send12.txt");
            InputStreamReader InputRead = new InputStreamReader(fileIn);
            char[] inputBuffer = new char[READ_BLOCK_SIZE];
            String s = "";
            int charRead;

            while ((charRead = InputRead.read(inputBuffer))>0) {
                String readstring = String.copyValueOf(inputBuffer,0,charRead);
                s +=readstring;
            }
            InputRead.close();
            readDataToList(context, s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readDataToList(Context context, String data) {
        Toast.makeText(context, "List Process", Toast.LENGTH_SHORT).show();

        String[] ListItems;
        ListItems = data.split("\n");

        try {
            //String[] mobileArray = {"Android","IPhone","WindowsMobile","Blackberry", "WebOS","Ubuntu","Windows7","Max OS X"};
            ArrayAdapter adapter = new ArrayAdapter<String>(context, R.layout.sent_listview, ListItems);
            ListView listView = (ListView) findViewById(R.id.sent_listview);
            listView.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < ListItems.length ; i++) {
            if (!ListItems[i].isEmpty()) {
                Toast.makeText(context, "Sending : " + ListItems[i], Toast.LENGTH_SHORT).show();
                String[] separated = ListItems[i].split(" : ");
                sendUtil.sendSMS(context, separated[0], separated[1], Integer.parseInt(separated[2]));
                removeLineOnTextfile(separated[0] + " : " + separated[1] + " : " + separated[2]);
                ReadFile(context);
            }
        }
    }




    public void removeLineOnTextfile(String lineToRemove) {
        File inputFile = new File(getFilesDir()+"/send12.txt");
        File tempFile = new File(getFilesDir()+"/send12.txt");

        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

            String currentLine;

            while((currentLine = reader.readLine()) != null) {
                // trim newline when comparing with lineToRemove
                String trimmedLine = currentLine.trim();
                if (!trimmedLine.equals("")) {
                    if(trimmedLine.equals(lineToRemove)) continue;
                    writer.write(currentLine + System.getProperty("line.separator"));
                }
            }
            writer.close();
            reader.close();

//            boolean successful = tempFile.renameTo(inputFile);
//            Toast.makeText(getApplicationContext(), "Delete: " + lineToRemove, Toast.LENGTH_SHORT).show();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //END OF SEND FUNCTION


    //RECEIVING FUNCTION
    private  boolean checkPermission(String permission){
        int checkPermission = ContextCompat.checkSelfPermission(this, permission);
        return checkPermission == PackageManager.PERMISSION_GRANTED;
    }

//    public void delete(){
//        if (deleteSMS()) {
//            refreshInbox();
//        } else {
//            Toast.makeText(mContext, "Sorry we can't delete messages.", Toast.LENGTH_LONG).show();
//            refreshInbox();
//        }
//    }
//
//    private boolean deleteSMS() {
//        boolean isDeleted;
//        try {
//            mContext.getContentResolver().delete(Uri.parse("content://sms/"), null, null);
//            isDeleted = true;
//        } catch (Exception ex) {
//            isDeleted = false;
//        }
//        return isDeleted;
//    }

    public void deleteSMS(Context context, String message, String number) {
        try {
            Toast.makeText(context, "Deleting SMS from inbox", Toast.LENGTH_SHORT).show();
            Uri uriSms = Uri.parse("content://sms/inbox");
            Cursor c = context.getContentResolver().query(uriSms,
                    new String[] { "_id", "thread_id", "address",
                            "person", "date", "body" }, null, null, null);

            if (c != null && c.moveToFirst()) {
                do {
                    long id = c.getLong(0);
                    long threadId = c.getLong(1);
                    String address = c.getString(2);
                    String body = c.getString(5);

                    if (message.equals(body) && address.equals(number)) {
                        Toast.makeText(context, "Deleting SMS with id: " + threadId, Toast.LENGTH_SHORT).show();
                        context.getContentResolver().delete(Uri.parse("content://sms/" + id), null, null);
                    }
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Toast.makeText(context, "Could not delete SMS from inbox: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    public  void refreshInbox(){
        ContentResolver cResolver = getContentResolver();
        Cursor smsInboxCursor = cResolver.query(Uri.parse("content://sms/inbox"),null,null,null,null);
        int indexBody = smsInboxCursor.getColumnIndex("body");
        int indexAddress = smsInboxCursor.getColumnIndex("address");
        if (indexBody < 0 || !smsInboxCursor.moveToFirst()) return;
        arrayAdapter.clear();
        do{
            String date =  smsInboxCursor.getString(smsInboxCursor.getColumnIndex("date"));
            Long timestamp = Long.parseLong(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timestamp);

            Date finaldate = calendar.getTime();

            final String strDateFormate = "yyyy-MM-dd hh:mm:ss";

            String mDay = DateFormat.format(strDateFormate, finaldate) + "";

            str = smsInboxCursor.getString(indexAddress) + " : " + smsInboxCursor.getString(indexBody) + " : " + mDay;

            arrayAdapter.add(str);
        }while (smsInboxCursor.moveToNext());
    }

//    public  void refreshInbox(){
//        ContentResolver cResolver = getContentResolver();
//        Cursor smsInboxCursor = cResolver.query(Uri.parse("content://sms/inbox"),null,null,null,null);
//        int indexBody = smsInboxCursor.getColumnIndex("body");
//        int indexAddress = smsInboxCursor.getColumnIndex("address");
//        if (indexBody < 0 || !smsInboxCursor.moveToFirst()) return;
////        arrayAdapter.clear();
//        do{
//            String date =  smsInboxCursor.getString(smsInboxCursor.getColumnIndex("date"));
//            Long timestamp = Long.parseLong(date);
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTimeInMillis(timestamp);
//
//            Date finaldate = calendar.getTime();
//
//            final String strDateFormate = "yyyy-MM-dd hh:mm:ss";
//
//            String mDay = DateFormat.format(strDateFormate, finaldate) + "";
//
//            str = smsInboxCursor.getString(indexAddress) + " : " + smsInboxCursor.getString(indexBody) + " : " + mDay;
//
//            arrayAdapter.add(str);
//        }while (smsInboxCursor.moveToNext());
//
//    }

    public void sendtodatabase(){
        try {
            JSONObject obj;
            JSONArray jsonArray = new JSONArray();
            ArrayList<String> mylist;
            mylist = new ArrayList<>(getSMS());
            for (int i = 0; i < mylist.size(); i++) {
                String[] separated = mylist.get(i).split(" ] ");
                try {
                    obj = new JSONObject();
                    obj.put("number", separated[0]);
                    obj.put("message", separated[1]);
                    obj.put("datetime", separated[2]);
                    jsonArray.put(obj);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            String jsonStr = jsonArray.toString();
            CharSequence array = jsonStr;
            Toast.makeText(getApplicationContext(), array, Toast.LENGTH_LONG).show();
            Submit((String) array);
        }
        catch (Exception z) {
            Toast.makeText(getApplicationContext(),"App crashed" + " " + z , Toast.LENGTH_SHORT).show();
        }

    }

    public void Receivemessages(String data) {
        try {
            JSONObject obj = null;
            JSONArray jsonArray = new JSONArray();
            ArrayList<String> mylist;
            mylist = new ArrayList<>();
            mylist.add(data);
            for (int i = 0; i < mylist.size(); i++) {
                String[] separated = mylist.get(i).split(" : ");
                try {
                    obj = new JSONObject();
                    obj.put("number", separated[0]);
                    obj.put("message", separated[1]);
                    obj.put("datetime", separated[2]);
                    obj.put("status", separated[3]);
                    jsonArray.put(obj);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            String jsonStr = jsonArray.toString();
            CharSequence array = jsonStr;
            Toast.makeText(getApplicationContext(), array, Toast.LENGTH_LONG).show();

            Submit((String) array);

        }
        catch (Exception z) {
            Toast.makeText(getApplicationContext(),"App crashed" + " " + z , Toast.LENGTH_SHORT).show();
        }

    }
    private void Submit(String data)
    {
        final String savedata = data;
        String URL="http://172.26.154.15/rest_api/insert_test.php";
//        String URL="http://172.26.154.15:8000/api/messages";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Toast.makeText(getApplicationContext(),"Successfully saved to database", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Submitted data: " + savedata);
                    JSONArray json = new JSONArray(savedata);
                    for(int i=0; i<json.length(); i++){
                        JSONObject e = json.getJSONObject(i);
                        Log.d(TAG, "Submitted data" + e.getString("message") + " : " + e.getString("number"));
                        deleteSMS(getApplicationContext(), e.getString("message"), e.getString("number"));
                    }
//                    arrayAdapter.clear();
                    arrayAdapter.notifyDataSetChanged();
                    viewbox(getApplicationContext());
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),"Could not save to database",Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Submitted data: " + savedata);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                refreshInbox();
                Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {

                    return savedata == null ? null : savedata.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    return null;
                }
            }

        };
        requestQueue.add(stringRequest);
    }

    public void updateList(final String smsMsg, int dels , String smsmessage, String smsnumber, String smsdate)
    {

        dates = smsdate;
        Receivemessages(smsMsg);
        viewbox(this);

    }

    public void viewbox(Context context) {
        arrayAdapter = new ArrayAdapter(context,android.R.layout.simple_list_item_1, smsMsgList);
        try {
            lvSMS = (ListView) findViewById(R.id.received_listview);
            smsMsgList.add("vtvtvtvtvt");
            lvSMS.setAdapter(arrayAdapter);
            arrayAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public List<String> getSMS(){
        List<String> sms = new ArrayList<String>();
        Uri uriSMSURI = Uri.parse("content://sms/inbox");
        Cursor cur = getContentResolver().query(uriSMSURI, null, null, null, null);

        while (cur != null && cur.moveToNext()) {
            String address = cur.getString(cur.getColumnIndex("address"));
            String body = cur.getString(cur.getColumnIndexOrThrow("body"));
            String date =  cur.getString(cur.getColumnIndex("date"));
            Long timestamp = Long.parseLong(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timestamp);
            Date finaldate = calendar.getTime();
            final String strDateFormate = "yyyy-MM-dd hh:mm:ss";

            String mDay = DateFormat.format(strDateFormate, finaldate) + "";
            sms.add(address + " ] " + body + " ] " + mDay);
        }

        if (cur != null) {
            cur.close();
        }
        return sms;
    }

    public String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

}
