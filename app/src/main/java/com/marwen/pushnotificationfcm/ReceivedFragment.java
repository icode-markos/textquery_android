package com.marwen.pushnotificationfcm;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReceivedFragment extends Fragment {
    Context mContext;

    private String str;

    ArrayList<String> smsMsgList = new ArrayList<String>();
    ArrayAdapter arrayAdapter;


    public ReceivedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_recieved, container, false);

        if(ContextCompat.checkSelfPermission(getActivity(), "android.permission.READ_SMS") != PackageManager.PERMISSION_GRANTED) {
            final int REQUEST_CODE_ASK_PERMISSIONS = 123;
            ActivityCompat.requestPermissions(getActivity(), new String[]{"android.permission.READ_SMS"}, REQUEST_CODE_ASK_PERMISSIONS);
        }

        if (getArguments() != null) {
            String mParam1 = getArguments().getString("params");
            Toast.makeText(getActivity(), mParam1, Toast.LENGTH_LONG).show();
        }

        View view = inflater.inflate(R.layout.fragment_recieved, container, false);

        arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, smsMsgList);
        ListView lvSMS = (ListView) view.findViewById(R.id.received_listview);
        String wew = "fassa";

        lvSMS.setAdapter(arrayAdapter);
        refreshInbox();
        smsMsgList.add(wew);


        return view;
    }

    public  void refreshInbox(){
        ContentResolver cResolver = getActivity().getContentResolver();
        Cursor smsInboxCursor = cResolver.query(Uri.parse("content://sms/inbox"),null,null,null,null);
        int indexBody = smsInboxCursor.getColumnIndex("body");
        int indexAddress = smsInboxCursor.getColumnIndex("address");
        if (indexBody < 0 || !smsInboxCursor.moveToFirst()) return;
        arrayAdapter.clear();
        do{

            String date =  smsInboxCursor.getString(smsInboxCursor.getColumnIndex("date"));
            Long timestamp = Long.parseLong(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timestamp);
            Date finaldate = calendar.getTime();

            final String strDateFormate = "dd/MM/yyyy h:mm:ss";
            String mDay = DateFormat.format(strDateFormate, finaldate) + "";
            str = smsInboxCursor.getString(indexAddress)+" ] "+
                    smsInboxCursor.getString(indexBody) + " ] " + mDay;
            arrayAdapter.add(str);
        }while (smsInboxCursor.moveToNext());
    }

    private int deleteMessage() {
        Uri deleteUri = Uri.parse("content://sms");
        int count = 0;
        @SuppressLint("Recycle") Cursor c = getActivity().getContentResolver().query(deleteUri, null, null, null, null);

        while (c.moveToNext()) {
            try {
                String pid = c.getString(0); // Get id;
                String uri = "content://sms/" + pid;
                count = getActivity().getContentResolver().delete(Uri.parse(uri),
                        null, null);
            } catch (Exception e) {
            }
        }
        return count;
    }

}
